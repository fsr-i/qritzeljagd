from app import create_app, models
import logging


def __setup_logging():
    logging.basicConfig(format="[%(asctime)s] [%(process)d] [%(levelname)s] %(message)s", datefmt="%Y-%m-%d %H:%M:%S %z", level=logging.INFO)

def shutdown():
    logging.info("Enough fun. Let's return to work.")

def startup():
    logging.info("Let's do some QRitzeljagd.")

__setup_logging()
app = create_app()