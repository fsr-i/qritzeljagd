from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from . import mongo


class Challenge(mongo.Document):
    name = mongo.StringField(max_length=500, unique=True, required=True)
    description = mongo.StringField(max_length=500, required=True)
    content = mongo.StringField(max_length=999999999, required=True)

class Station(mongo.Document):
    name = mongo.StringField(max_length=500, unique=True, required=True)
    location = mongo.StringField(max_length=500, required=True)
    content = mongo.StringField(required=True)
    hint = mongo.StringField(required=True)
    challenge = mongo.LazyReferenceField(Challenge)

class Track(mongo.Document):
    name = mongo.StringField(max_length=500, required=True)
    stations = mongo.ListField(mongo.LazyReferenceField(Station))

class QrCode(mongo.Document):
    code = mongo.StringField(max_length=500, unique=True, required=True)

class Waypoint(mongo.Document):
    qr_code = mongo.ReferenceField(QrCode)
    station = mongo.LazyReferenceField(Station)
    track = mongo.LazyReferenceField(Track)

class Playgroup(mongo.Document):
    name = mongo.StringField(max_length=500, required=True, unique=False)
    track = mongo.LazyReferenceField(Track)
    max_players = mongo.IntField(required=False)
    visited_waypoints = mongo.ListField(mongo.LazyReferenceField(Waypoint))
    solved_current_challenge = mongo.BooleanField(default=False)

class Person(mongo.Document):
    uuid = mongo.UUIDField(max_length=500, required=True, unique=True)
    playgroup = mongo.LazyReferenceField(Playgroup)
    note = mongo.StringField(max_length=500, default='')

class MasterAccount(mongo.Document, UserMixin):
    username = mongo.StringField(max_length=50, required=True, unique=True)
    password = mongo.StringField(max_length=200, required=False, unique=False)

    def set_password(self, password):
        self.password = generate_password_hash(password, method="sha256")

    def check_password(self, password):
        return check_password_hash(self.password, password)

class Printout(mongo.Document):
    station = mongo.LazyReferenceField(Station, required=True)
    tracks = mongo.ListField(mongo.LazyReferenceField(Track))
    codes = mongo.ListField(mongo.StringField(max_length=500))
    note = mongo.StringField(max_length=500, default='')

    def check_if_outdated(self):
        irrelevant_tracks = []
        tracks_lacking_waypoints = []
        tracks_with_multiple_waypoints = []
        unambiguous_codes = []
        missing_codes = []

        for track in self.tracks:
            try:
                track = track.fetch()
            except Track.DoesNotExist:
                irrelevant_tracks.append(track)
                continue

            if self.station not in track.stations:
                irrelevant_tracks.append(track)
            else:
                waypoints = Waypoint.objects(station=self.station, track=track)
                if len(waypoints) == 0:
                    tracks_lacking_waypoints.append(track)
                elif len(waypoints) > 1:
                    tracks_with_multiple_waypoints.append(track)
                else:
                    code = waypoints[0].qr_code.code
                    if code in self.codes:
                        unambiguous_codes.append(code)
                    else:
                        missing_codes.append(code)

        orphan_codes = list(set(self.codes).difference(unambiguous_codes).difference(missing_codes))

        no_wrong_info = len(orphan_codes) == 0
        proper_print = len(irrelevant_tracks) == 0 and len(tracks_lacking_waypoints) == 0 and len(tracks_with_multiple_waypoints) == 0 and len(unambiguous_codes) == len(self.codes) and len(missing_codes) == 0 and len(orphan_codes) == 0

        return dict(
            no_wrong_info=no_wrong_info,
            proper_print=proper_print,

            irrelevant_tracks=irrelevant_tracks,
            tracks_lacking_waypoints=tracks_lacking_waypoints,
            tracks_with_multiple_waypoints=tracks_with_multiple_waypoints,
            unambiguous_codes=unambiguous_codes,
            missing_codes=missing_codes,
            orphan_codes=orphan_codes)
