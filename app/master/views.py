from flask import Flask, render_template, redirect, request, flash
from flask_babel import lazy_gettext as _l
from flask_login import current_user, login_user, login_required, logout_user
from flask_weasyprint import HTML, render_pdf
from . import master
from ..models import MasterAccount, Station, Challenge, Track, Waypoint, QrCode, Playgroup, Person, Printout
from .. import login_manager
import hashlib
import random
from dotenv import load_dotenv
import os

load_dotenv()

QR_CODE_BASE_URL = '{}://{}{}'.format(os.getenv('PREFERRED_URL_SCHEME'), os.getenv('SERVER_NAME'), os.getenv('APP_ROOT'))


#
# Routes
#

@master.route("/master", methods=["GET"])
def index():
    return redirect("/master/login")

@master.route("/master/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect("/master/overview")
    
    if "login_btn" in request.form and request.method == "POST":
        input_username = request.form["username"]
        input_password = request.form["password"]

        try:
            user = MasterAccount.objects.get(username=input_username)
            if user.check_password(input_password) is True:
                login_user(user)
                next = request.args.get("next")
                return redirect(next or "/master/overview")
        except MasterAccount.DoesNotExist:
            pass

        flash(_l("Wrong credentials."))

    return render_template("/master/login.html")

@master.route("/master/overview", methods=["GET", "POST"])
@login_required
def overview():
    return render_template("/master/overview.html")

@master.route("/master/stations", methods=["GET", "POST"])
@login_required
def stations():
    boxes = Station.objects.all()
    for box in boxes:
        box.info = box.location

    return render_template("/master/stations.html",
                           boxes=boxes)

@master.route("/master/stations/add", methods=["GET", "POST"])
@login_required
def stations_add():
    #challenge_choices = Challenge.objects.all()
    challenge_choices = list(map(lambda x: x.name, Challenge.objects))
    # add an option for no playgroup at all
    challenge_choices.insert(0, "")
    preselect = ""

    if "cancel_btn" in request.form and request.method == "POST":
        return redirect("/master/stations")
    elif "save_btn" in request.form and request.method == "POST":
        input_name = request.form["name"]
        input_location = request.form["location"]
        input_content = request.form["content"]
        input_hint = request.form["hint"]
        select_challenge = request.form["select_challenge"]

        if input_name == "" or input_location == "" or input_content == "":
            flash(_l("Please fill out all forms."))
            return render_template("/master/stations_edit.html",
                                   value_name=input_name,
                                   value_info=input_location,
                                   value_content=input_content,
                                   value_hint=input_hint,
                                   challenge_choices=challenge_choices,
                                   preselect=select_challenge,
                                   editing=False)

        challenge = None
        if select_challenge != "":
            for challenge_pick in Challenge.objects:
                if challenge_pick.name == select_challenge:
                    challenge = challenge_pick
                    break

        try:
            Station(name=input_name,
                    location=input_location,
                    content=input_content,
                    hint=input_hint,
                    challenge=challenge).save()
        except IntegrityError:
            #db.session.rollback()
            flash(_l("Station with this name already exists."))
            return render_template("/master/stations_edit.html",
                                   value_name=input_name,
                                   value_info=input_location,
                                   value_content=input_content,
                                   value_hint=input_hint,
                                   challenge_choices=challenge_choices,
                                   preselect=select_challenge,
                                   editing=False)
        except DataError:
            #db.session.rollback()
            flash(_l("Data error occured. Check the values for compatibility with the data types in the database."))
            return render_template("/master/stations_edit.html",
                                   value_name=input_name,
                                   value_info=input_location,
                                   value_content=input_content,
                                   value_hint=input_hint,
                                   challenge_choices=challenge_choices,
                                   preselect=select_challenge,
                                   editing=False)
        except:
            #db.session.rollback()
            flash(_l("An error occured."))
        
        return redirect("/master/stations")

    return render_template("/master/stations_edit.html",
                           value_name="",
                           value_info="",
                           value_content="",
                           value_hint="",
                           challenge_choices=challenge_choices,
                           preselect="",
                           editing=False)

@master.route("/master/stations/<station_id>", methods=["GET", "POST"])
@login_required
def stations_show(station_id):
    station = Station.objects.with_id(station_id)
    if not station.challenge:
        preselect = ""
    else:
        preselect = station.challenge.fetch().name

    challenge_choices = Challenge.objects.all()
    challenge_choices = list(map(lambda x: x.name, challenge_choices))
    # add an option for no playgroup at all
    challenge_choices.insert(0, "")

    input_hint = station.hint
    if input_hint is None:
        input_hint = ""

    if station is None:
        flash(_l("Invalid Station."))
        return redirect("/master/stations")
    
    if "cancel_btn" in request.form and request.method == "POST":
        return redirect("/master/stations")
    elif "delete_btn" in request.form and request.method == "POST":
        try:
            for track in Track.objects(stations=station):
                track.stations.remove(station)
                track.save()

            for waypoint in Waypoint.objects(station=station):
                waypoint.qr_code.delete()
                waypoint.delete()

            station.delete()
            flash(_l("Station deleted."))
        except:
            #db.session.rollback()
            flash(_l("Error while deleting station."))

        return redirect("/master/stations")
    elif "save_btn" in request.form and request.method == "POST":
        input_name = request.form["name"]
        input_location = request.form["location"]
        input_content = request.form["content"]
        input_hint = request.form["hint"]
        select_challenge = request.form["select_challenge"]

        if input_name == "" or input_location == "" or input_content == "":
            flash(_l("Please fill out all forms."))
            return render_template("/master/stations_edit.html",
                                   value_name=input_name,
                                   value_info=input_location,
                                   value_content=input_content,
                                   value_hint=input_hint,
                                   challenge_choices=challenge_choices,
                                   preselect=select_challenge,
                                   editing=True)

        if select_challenge != "":
            challenge = Challenge.objects.get(name=select_challenge)
        else:
            challenge = None

        try:
            station.name = input_name
            station.location = input_location
            station.content = input_content
            station.hint = input_hint
            station.challenge = challenge

            station.save()
        except DataError:
            #db.session.rollback()
            flash(_l("Data error occured. Check the values for compatibility with the data types in the database."))
            return render_template("/master/stations_edit.html",
                                   value_name=input_name,
                                   value_info=input_location,
                                   value_content=input_content,
                                   value_hint=input_hint,
                                   challenge_choices=challenge_choices,
                                   preselect=select_challenge,
                                   editing=True)
        except:
            #db.session.rollback()
            flash(_l("An error occured."))
        
        return redirect("/master/stations")
    
    return render_template("/master/stations_edit.html",
                            value_name=station.name,
                            value_info=station.location,
                            value_content=station.content,
                            value_hint=input_hint,
                            challenge_choices=challenge_choices,
                            preselect=preselect,
                            editing=True)

@master.route("/master/challenges", methods=["GET", "POST"])
@login_required
def challenges():
    boxes = Challenge.objects.all()
    for box in boxes:
        box.info = box.description

    return render_template("/master/challenges.html",
                           boxes=boxes)

@master.route("/master/challenges/add", methods=["GET", "POST"])
@login_required
def challenges_add():
    if "cancel_btn" in request.form and request.method == "POST":
        return redirect("/master/challenges")
    elif "save_btn" in request.form and request.method == "POST":
        input_name = request.form["name"]
        input_description = request.form["description"]
        input_content = request.form["content"]

        if input_name == "" or input_description == "" or input_content == "":
            flash(_l("Please fill out all forms."))
            return render_template("/master/challenges_edit.html",
                                   value_name=input_name,
                                   value_info=input_description,
                                   value_content=input_content,
                                   editing=False)

        try:
            Challenge(name=input_name,
                      description=input_description,
                      content=input_content).save()
        except IntegrityError:
            #db.session.rollback()
            flash(_l("Station with this name already exists."))
            return render_template("/master/challenges_edit.html",
                                   value_name=input_name,
                                   value_info=input_description,
                                   value_content=input_content,
                                   editing=False)
        except DataError:
            #db.session.rollback()
            flash(_l("Data error occured. Check the values for compatibility with the data types in the database."))
            return render_template("/master/challenges_edit.html",
                                   value_name=input_name,
                                   value_info=input_description,
                                   value_content=input_content,
                                   editing=False)
        except:
            #db.session.rollback()
            flash(_l("An error occured."))

        return redirect("/master/challenges")

    return render_template("/master/challenges_edit.html",
                           value_name="",
                           value_info="",
                           value_content="",
                           editing=False)

@master.route("/master/challenges/<challenge_id>", methods=["GET", "POST"])
@login_required
def challenges_show(challenge_id):
    challenge = Challenge.objects.with_id(challenge_id)

    if challenge is None:
        flash(_l("Invalid Station."))
        return redirect("/master/challenges")

    if "cancel_btn" in request.form and request.method == "POST":
        return redirect("/master/challenges")
    elif "delete_btn" in request.form and request.method == "POST":
        try:
            stations = Station.objects(challenge=challenge)
            for station in stations:
                # set affected stations to not have a challenge
                station.challenge = None
                station.save()

            flash(_l("These stations are now without a challenge: {}")
                .format(', '.join(map(lambda x: x.name, stations))))

            challenge.delete()
            flash(_l("Challenge deleted."))
        except:
            #db.session.rollback()
            flash(_l("Error while deleting challenge."))

        return redirect("/master/challenges")
    elif "save_btn" in request.form and request.method == "POST":
        input_name = request.form["name"]
        input_description = request.form["description"]
        input_content = request.form["content"]

        if input_name == "" or input_description == "" or input_content == "":
            flash(_l("Please fill out all forms."))
            return render_template("/master/challenges_edit.html",
                                   value_name=input_name,
                                   value_info=input_description,
                                   value_content=input_content,
                                   editing=True)

        try:
            challenge.name = input_name
            challenge.description = input_description
            challenge.content = input_content

            challenge.save()
        except DataError:
            #db.session.rollback()
            flash(_l("Data error occured. Check the values for compatibility with the data types in the database."))
            return render_template("/master/challenges_edit.html",
                                   value_name=input_name,
                                   value_info=input_description,
                                   value_content=input_content,
                                   editing=True)
        except:
            #db.session.rollback()
            flash(_l("An error occured."))

        return redirect("/master/challenges")

    return render_template("/master/challenges_edit.html",
                            value_name=challenge.name,
                            value_info=challenge.description,
                            value_content=challenge.content,
                            editing=True)

@master.route("/master/tracks", methods=["GET", "POST"])
@login_required
def tracks():
    tracks = Track.objects.all()
    for track in tracks:
        track.waypoint_count = Waypoint.objects(track=track).count()
        track.playgroup_count = Playgroup.objects(track=track).count()
        track.info = _l("%(waypoint_count)s waypoints", waypoint_count=track.waypoint_count) + \
                     "\n" + \
                     _l("%(playgroup_count)s playgroups", playgroup_count=track.playgroup_count)

    return render_template("/master/tracks.html",
                           boxes=tracks)

@master.route("/master/tracks/add", methods=["GET", "POST"])
@login_required
def tracks_add():
    if "cancel_btn" in request.form and request.method == "POST":
        return redirect("/master/tracks")
    elif "save_btn" in request.form and request.method == "POST":
        input_name = request.form["name"]

        if input_name == "":
            flash(_l("Please fill out all forms."))
            return render_template("/master/tracks_edit.html",
                                   value_name=input_name,
                                   editing=False)

        try:
            track = Track(name=input_name).save()

            def rollback_track():
                try:
                    track.delete()
                except:
                    #db.session.rollback()
                    flash(_l("An error occured rolling back the track after f*cking up with the matching playgroup. You're on your own now."))

            try:
                Playgroup(name=input_name, track=track, max_players=5).save()
            except IntegrityError:
                #db.session.rollback()
                flash(_l("Playgroup with this name already exists."))
                rollback_track()
                return render_template("/master/tracks_edit.html",
                                       value_name=input_name,
                                       editing=False)
            except DataError:
                #db.session.rollback()
                flash(_l("Data error occured when creating matching playgroup. Check the values for compatibility with the data types in the database."))
                rollback_track()
                return render_template("/master/tracks_edit.html",
                                       value_name=input_name,
                                       editing=False)
            except:
                #db.session.rollback()
                flash(_l("An error occured while creating a playgroup for the new track."))
                rollback_track()

        except IntegrityError:
            #db.session.rollback()
            flash(_l("Track with this name already exists."))
            return render_template("/master/tracks_edit.html",
                                   value_name=input_name,
                                   editing=False)
        except DataError:
            #db.session.rollback()
            flash(_l("Data error occured. Check the values for compatibility with the data types in the database."))
            return render_template("/master/tracks_edit.html",
                                   value_name=input_name,
                                   editing=False)
        except:
            #db.session.rollback()
            flash(_l("An error occured."))
        
        return redirect("/master/tracks")

    return render_template("/master/tracks_edit.html",
                           value_name="",
                           editing=False)

@master.route("/master/tracks/<track_id>", methods=["GET", "POST"])
@login_required
def tracks_show(track_id):
    if "cancel_btn" in request.form and request.method == "POST":
        return redirect("/master/tracks")

    track = Track.objects.with_id(track_id)
    playgroups = Playgroup.objects(track=track)

    if track is None:
        flash(_l("Invalid Track."))
        return redirect("/master/tracks")

    waypoints = Waypoint.objects(track=track)
    waypoint_objs = list(map(lambda x: create_waypoint_object(waypoint=next(filter(lambda y: y.station == x, waypoints))), track.stations))

    if "delete_btn" in request.form and request.method == "POST":
        #try:
        for waypoint in waypoints:
            waypoint.qr_code.delete()
            waypoint.delete()

        for playgroup in playgroups:
            playgroup.track = None
            playgroup.save()

        track.delete()
        flash(_l("Track deleted."))
        #except Exception:
        #    #db.session.rollback()
        #    flash(_l("Error while deleting track."))

        return redirect("/master/tracks")
    elif "save_btn" in request.form and request.method == "POST":
        input_name = request.form["name"]

        if input_name == "":
            flash(_l("Please fill out all forms."))
            return render_template("/master/tracks_edit.html",
                                   value_name=input_name,
                                   editing=True,
                                   waypoints=waypoint_objs)

        try:
            track.name = input_name
            track.save()
        except DataError:
            #db.session.rollback()
            flash(_l("Data error occured. Check the values for compatibility with the data types in the database."))
            return render_template("/master/tracks_edit.html",
                                   value_name=input_name,
                                   editing=True,
                                   waypoints=waypoint_objs)
        except:
            #db.session.rollback()
            flash(_l("An error occured."))
        
        return redirect("/master/tracks")
    elif ("add_waypoint" in request.form or "apply_btn" in request.form) and request.method == "POST":
        input_name = request.form["name"]

        if "select_station" in request.form:
            select_station = request.form["select_station"]

            try:
                station = Station.objects.get(name=select_station)
                count = Waypoint.objects(track=track).count()

                proposed_code = str(station.id) + str(track.id) + str(random.random())
                code = hashlib.sha256(proposed_code.encode()).hexdigest()[:15]
                # TODO: possibility to enter custom code
                qr_code = QrCode(code=str(code)).save()

                waypoint = Waypoint(qr_code=qr_code,
                                    station=station,
                                    track=track).save()

                track.stations.append(station)
                track.save()

                waypoints = Waypoint.objects(track=track)
                waypoint_objs = list(map(lambda x: create_waypoint_object(waypoint=next(filter(lambda y: y.station == x, waypoints))), track.stations))

            except ValueError as ex:#Exception as ex:
                print(ex)
                #db.session.rollback()
                flash(_l("An error occured."))

        if "add_waypoint" in request.form:
            used_stations = list(map(lambda x: x.station.id, waypoints))
            stations = Station.objects(id__nin=used_stations)

            return render_template("/master/tracks_edit.html",
                                   value_name=input_name,
                                   editing=True,
                                   waypoints=waypoint_objs,
                                   add_waypoint=True,
                                   stations=stations)
        else:
            return render_template("/master/tracks_edit.html",
                                   value_name=input_name,
                                   editing=True,
                                   waypoints=waypoint_objs)

    return render_template("/master/tracks_edit.html",
                            value_name=track.name,
                            editing=True,
                            waypoints=waypoint_objs)

@master.route("/master/waypoints/<track_id>/<station_id>/delete", methods=["GET", "POST"])
@login_required
def waypoint_delete(track_id, station_id):
    track = Track.objects.with_id(track_id)
    station = Station.objects.with_id(station_id)

    try:
        track.stations.remove(station)
        track.save()

        waypoint = Waypoint.objects.get(track=track, station=station)
        waypoint.qr_code.delete()
        waypoint.delete()
    except Exception:
        #db.session.rollback()
        flash(_l("An error occured."))
    
    return redirect("/master/tracks/" + track_id)

@master.route("/master/waypoints/<track_id>/<station_id>/move_up", methods=["GET", "POST"])
@login_required
def waypoint_move_up(track_id, station_id):
    track = Track.objects.with_id(track_id)
    station = Station.objects.with_id(station_id)
    old_idx = track.stations.index(station)

    try:
        if old_idx < 1:
            flash(_l("Waypoint is already first in track."))
            return redirect("/master/tracks/" + track_id)

        s = track.stations
        s[old_idx-1], s[old_idx] = s[old_idx], s[old_idx-1]

        track.save()
    except Exception:
        #db.session.rollback()
        flash(_l("An error occured."))

    return redirect("/master/tracks/" + track_id)

@master.route("/master/waypoints/<track_id>/<station_id>/move_down", methods=["GET", "POST"])
@login_required
def waypoint_move_down(track_id, station_id):
    track = Track.objects.with_id(track_id)
    station = Station.objects.with_id(station_id)
    old_idx = track.stations.index(station)

    try:
        if old_idx >= len(track.stations):
            flash(_l("Waypoint is already last in track."))
            return redirect("/master/tracks/" + track_id)

        s = track.stations
        s[old_idx], s[old_idx+1] = s[old_idx+1], s[old_idx]

        track.save()
    except Exception:
        #db.session.rollback()
        flash(_l("An error occured."))

    return redirect("/master/tracks/" + track_id)

@master.route("/master/waypoints/<track_id>/<station_id>/download", methods=["GET", "POST"])
@login_required
def waypoint_download(track_id, station_id):
    track = Track.objects.with_id(track_id)
    station = Station.objects.with_id(station_id)
    waypoint = Waypoint.objects.get(track=track, station=station)
    qr_code = waypoint.qr_code

    if track is None or station is None:
        flash(_l("Invalid station or track."))
        redirect("/master/overview")
    
    html = render_template("/master/pdf/waypoint.html",
                           station_name=station.name,
                           track_name=track.name,
                           qr_code=QR_CODE_BASE_URL + "/waypoint/" + qr_code.code)

    return render_pdf(HTML(string=html),
                      download_filename=track.name.encode("ascii", errors="xmlcharrefreplace").decode() + ", " + station.name.encode("ascii", errors="xmlcharrefreplace").decode() + ".pdf")

@master.route("/master/playgroups", methods=["GET", "POST"])
@login_required
def playgroups():
    playgroup_objs = []
    playgroups = Playgroup.objects.all()
    for playgroup in playgroups:
        if playgroup.track is not None:
            track = playgroup.track.fetch()
            visited_idxs = map(lambda x: track.stations.index(x.fetch().station), playgroup.visited_waypoints)
            progress = max(visited_idxs, default=0)
            playgroup_objs.append(PlaygroupObject(playgroup.id, track.name, playgroup.name,
                progress, Waypoint.objects(track=track).count()))
        else:
            playgroup_objs.append(PlaygroupObject(playgroup.id, None, playgroup.name,
                None, None))

    return render_template("/master/playgroups.html",
                           boxes=playgroup_objs)

@master.route("/master/playgroups/add", methods=["GET", "POST"])
@login_required
def playgroups_add():
    if "cancel_btn" in request.form and request.method == "POST":
        return redirect("/master/playgroups")
    elif "save_btn" in request.form and request.method == "POST":
        input_name = request.form["name"]

        if input_name == "":
            flash(_l("Please fill out all forms."))
            return render_template("/master/playgroups_edit.html",
                                   value_name=input_name,
                                   editing=False)

        try:
            playgroup = Playgroup(name=input_name).save()

            def rollback_playgroup():
                try:
                    playgroup.delete()
                except:
                    #db.session.rollback()
                    flash(_l("An error occured rolling back the playgroup."))

        except IntegrityError:
            #db.session.rollback()
            flash(_l("Playgroup with this name already exists."))
            return render_template("/master/playgroups_edit.html",
                                   value_name=input_name,
                                   editing=False)
        except:
            #db.session.rollback()
            flash(_l("An error occured."))

        return redirect("/master/playgroups")

    return render_template("/master/playgroups_edit.html",
                           value_name="",
                           editing=False)

@master.route("/master/playgroups/<playgroup_id>", methods=["GET", "POST"])
@login_required
def playgroups_show(playgroup_id):
    if playgroup_id == "None":
        # TODO: redirect to persons overview not in a playgroup
        flash("This does not show a real playgroup. This is a temporary way to edit persons not assigned to a playgroup. The fields for Track, Name and Maximum Players won't do anything.")

        if ("cancel_btn" in request.form or "save_btn" in request.form) and request.method == "POST":
            return redirect("/master/playgroups")

        person_objs = []
        persons = Person.objects(playgroup=None)
        for person in persons:
            person.playgroup_name = ''
            person_objs.append(person)

        return render_template("/master/playgroups_edit.html",
                               value_track='',
                               value_name='',
                               value_max_players=0,
                               solved_current_challenge=False,
                               waypoints=list(),
                               persons=person_objs)

    playgroup = Playgroup.objects.with_id(playgroup_id)
    track = playgroup.track

    input_name = playgroup.name
    input_max_players = playgroup.max_players

    def get_waypoints(station):
        try:
            return Waypoint.objects.get(station=station, track=track)
        except Waypoint.DoesNotExist:
            return None

    if track is not None:
        track = track.fetch()
        input_track = track.name
        sorted_waypoints = list(map(get_waypoints, track.stations))
    else:
        input_track = None
        sorted_waypoints = list()

    waypoint_objs = list(map(lambda x: create_waypoint_object(playgroup=playgroup, track=track, waypoint=x), sorted_waypoints))

    # hint handling
    next_station_hint = ""
    next_station = get_next_station(playgroup)
    if next_station is not None:
        next_station = next_station.fetch()
        if next_station.hint is not None:
            next_station_hint = next_station.hint
    else:
        next_station_hint = _l("Congratulations! You made it to the end!")

    person_objs = []
    persons = Person.objects(playgroup=playgroup)
    for person in persons:
        person.playgroup_name = '' if person.playgroup is None else person.playgroup.fetch().name
        person_objs.append(person)

    if "cancel_btn" in request.form and request.method == "POST":
        return redirect("/master/playgroups")
    elif "save_btn" in request.form and request.method == "POST":
        input_name = request.form["name"]
        input_max_players = request.form["max_players"]

        if input_name == "" or input_max_players == "":
            flash(_l("Please fill out all forms."))
            return render_template("/master/playgroups_edit.html",
                                   value_track=input_track,
                                   value_name=input_name,
                                   value_max_players=input_max_players,
                                   solved_current_challenge=playgroup.solved_current_challenge,
                                   waypoints=waypoint_objs,
                                   next_station_hint=next_station_hint,
                                   persons=person_objs)

        select_update_progress = request.form["select_update_progress"]
        if select_update_progress == "[RESET TO ZERO]":
            playgroup.visited_waypoints = []
            # re-make waypoint objs
            waypoint_objs = list(map(lambda x: create_waypoint_object(playgroup=playgroup, track=track, waypoint=x), sorted_waypoints))
        elif select_update_progress != "":
            try:
                idx = list(map(lambda x: x and str(x.waypoint_id) == select_update_progress, waypoint_objs)).index(True)
            except ValueError:
                return render_template("/master/playgroups_edit.html",
                                       value_track=input_track,
                                       value_name=input_name,
                                       value_max_players=input_max_players,
                                       solved_current_challenge=playgroup.solved_current_challenge,
                                       waypoints=waypoint_objs,
                                       next_station_hint=next_station_hint,
                                       persons=person_objs)

            waypoints = sorted_waypoints[0:idx+1]
            playgroup.visited_waypoints = waypoints
            # re-make waypoint objs
            waypoint_objs = list(map(lambda x: create_waypoint_object(playgroup=playgroup, track=track, waypoint=x), sorted_waypoints))

        select_solved_current_challenge = request.form["select_solved_current_challenge"]
        if select_solved_current_challenge == "[RESET TO UNSOLVED]":
            playgroup.solved_current_challenge = False
        elif select_solved_current_challenge == "[SET TO SOLVED]":
            playgroup.solved_current_challenge = True

        try:
            playgroup.name = input_name
            playgroup.max_players = input_max_players
            playgroup.save()
        except IntegrityError:
            #db.session.rollback()
            flash(_l("A playgroup with this name already exists."))
        except:
            #db.session.rollback()
            flash(_l("An error occured."))

    return render_template("/master/playgroups_edit.html",
                           value_track=input_track,
                           value_name=input_name,
                           value_max_players=input_max_players,
                           solved_current_challenge=playgroup.solved_current_challenge,
                           waypoints=waypoint_objs,
                           next_station_hint=next_station_hint,
                           persons=person_objs)

@master.route("/master/persons/<person_uuid>/move", methods=["GET", "POST"])
@login_required
def person_move(person_uuid):
    try:
        person = Person.objects.get(uuid=person_uuid)
        original_playgroup = person.playgroup.fetch()
    except Person.DoesNotExist:
        original_playgroup = None

    if "cancel_btn" in request.form and request.method == "POST":
        return redirect("/master/playgroups/{}".format(original_playgroup.id))
    elif "save_btn" in request.form and request.method == "POST":
        input_note = request.form["note"]
        select_playgroup = request.form["select_playgroup"]

        try:
            person.note = input_note
            if select_playgroup == '':
                person.playgroup = None
            else:
                playgroup = Playgroup.objects.get(name=select_playgroup)
                person.playgroup = playgroup

            person.save()
        except Exception:
            #db.session.rollback()
            flash(_l("An error occured."))

        # redirect to the old playgroup
        if original_playgroup is None:
            return redirect("/master/playgroups/None")
        else:
            return redirect("/master/playgroups/{}".format(original_playgroup.id))
    else:
        if original_playgroup is None:
            preselect = ""
        else:
            preselect = original_playgroup.name

        playgroup_choices = Playgroup.objects.all()
        playgroup_choices = list(map(lambda x: x.name, playgroup_choices))
        # add an option for no playgroup at all
        playgroup_choices.insert(0, "")

    return render_template("/master/person_move.html",
                           playgroup_choices=playgroup_choices,
                           preselect=preselect,
                           person=person)

@master.route("/master/persons/<person_uuid>/delete", methods=["GET", "POST"])
@login_required
def person_delete(person_uuid):
    playgroup = None
    try:
        person = Person.objects.get(uuid=person_uuid)
        playgroup = person.playgroup
        person.delete()
    except Person.DoesNotExist:
        flash(_l("Person already did not exist."))
    except Exception:
        #db.session.rollback()
        flash(_l("An error occured."))

    return redirect("/master/playgroups/{}".format(playgroup.id))

@master.route("/master/logout")
@login_required
def logout():
    logout_user()
    return redirect("/master/login")

@master.route("/master/printouts", methods=["GET", "POST"])
@login_required
def printouts():
    boxes = []
    printouts = Printout.objects.all()

    def handle_tracks(track):
        try:
            t = track.fetch()
            track = t
        except Track.DoesNotExist:
            return None

    for printout in printouts:
        check = printout.check_if_outdated()
        tracks = list(filter(lambda x: x is not None, map(handle_tracks, printout.tracks)))
        boxes.append(dict(
            id=printout.id,
            station=printout.station.fetch(),
            tracks=tracks,
            codes=printout.codes,
            note=printout.note,

            no_wrong_info=check["no_wrong_info"],
            proper_print=check["proper_print"],

            irrelevant_tracks=check["irrelevant_tracks"],
            tracks_lacking_waypoints=check["tracks_lacking_waypoints"],
            tracks_with_multiple_waypoints=check["tracks_with_multiple_waypoints"],
            unambiguous_codes=check["unambiguous_codes"],
            missing_codes=check["missing_codes"],
            orphan_codes=check["orphan_codes"]
        ))

    return render_template("/master/printouts.html",
                           boxes=boxes)

@master.route("/master/printouts/add", methods=["GET", "POST"])
@login_required
def printouts_add():
    station_choices = Station.objects.all()
    station_choices = list(map(lambda x: x.name, station_choices))
    preselect = ""

    if "cancel_btn" in request.form and request.method == "POST":
        return redirect("/master/printouts")
    elif "save_btn" in request.form and request.method == "POST":
        select_station = request.form["select_station"]
        input_note = request.form["note"]

        station = Station.objects.get(name=select_station)

        try:
            Printout(station=station,
                    note=input_note).save()
        except:
            #db.session.rollback()
            flash(_l("An error occured."))

        return redirect("/master/printouts")

    return render_template("/master/printouts_edit.html",
                            station_choices=station_choices,
                            preselect=preselect,
                            value_note="",
                            waypoints=[],
                            editing=True)

@master.route("/master/printouts/<printout_id>", methods=["GET", "POST"])
@login_required
def printouts_show(printout_id):
    printout = Printout.objects.with_id(printout_id)
    station = printout.station.fetch()
    preselect = station.name

    station_choices = Station.objects.all()
    station_choices = list(map(lambda x: x.name, station_choices))

    if printout is None:
        flash(_l("Invalid Printout."))
        return redirect("/master/printouts")

    waypoints = Waypoint.objects(station=station, track__in=printout.tracks)
    waypoint_objs = list(map(lambda x: create_waypoint_object(waypoint=x), waypoints))
    tracks = list(map(lambda x: x.id, printout.tracks))
    waypoint_objs.sort(key=lambda x: tracks.index(x.track_id))

    if "cancel_btn" in request.form and request.method == "POST":
        return redirect("/master/printouts")
    elif "delete_btn" in request.form and request.method == "POST":
        try:
            printout.delete()
            flash(_l("Printout deleted."))
        except:
            #db.session.rollback()
            flash(_l("Error while deleting printout."))

        return redirect("/master/printouts")
    elif "save_btn" in request.form and request.method == "POST":
        select_station = request.form["select_station"]
        input_note = request.form["note"]

        station = Station.objects.get(name=select_station)

        try:
            printout.station = station
            printout.note = input_note

            printout.save()
        except DataError:
            #db.session.rollback()
            flash(_l("Data error occured. Check the values for compatibility with the data types in the database."))
            return render_template("/master/printouts_edit.html",
                                   station_choices=station_choices,
                                   value_note=input_note,
                                   preselect=select_station,
                                   waypoints=waypoint_objs,
                                   editing=True)
        except:
            #db.session.rollback()
            flash(_l("An error occured."))

        return redirect("/master/printouts")
    elif "add_waypoint" in request.form and request.method == "POST":
        select_station = request.form["select_station"]
        input_note = request.form["note"]

        station = Station.objects.get(name=select_station)
        tracks_with_station = Track.objects(stations__contains=station)
        waypoints = Waypoint.objects(station=station, track__in=tracks_with_station)
        waypoint_objs = list(map(lambda x: create_waypoint_object(waypoint=x), waypoints))
        # Shuffle the position of the qr codes for tracks,
        # but deterministically and repeatably
        waypoint_objs.sort(key=lambda x: x.track_id)
        rdm = random.Random(printout_id)
        rdm.shuffle(waypoint_objs)

        try:
            printout.station = station
            printout.tracks = list(map(lambda x: x.track, waypoint_objs))
            printout.codes = list(map(lambda x: x.code, waypoint_objs))

            printout.save()
            flash(_l("The codes were set to those of all {} fitting waypoints.").format(len(waypoint_objs)))
        except:
            #db.session.rollback()
            flash(_l("An error occured."))
    elif "print_btn" in request.form:
        html = render_template("/master/pdf/printout.html",
                               codes=list(map(lambda x: QR_CODE_BASE_URL + "/waypoint/" + x, printout.codes)))

        return render_pdf(HTML(string=html),
                          download_filename=station.name.encode("ascii", errors="xmlcharrefreplace").decode() + " " + printout.note + ".pdf")

    return render_template("/master/printouts_edit.html",
                            station_choices=station_choices,
                            preselect=preselect,
                            value_note=printout.note,
                            waypoints=waypoint_objs,
                            editing=True)

@master.route("/master/printouts/<printout_id>/preview", methods=["GET", "POST"])
@login_required
def printouts_preview(printout_id):
    printout = Printout.objects.with_id(printout_id)
    station = printout.station.fetch()

    if printout is None:
        flash(_l("Invalid Printout."))
        return redirect("/master/printouts")

    return render_template("/master/pdf/printout.html",
                           station_name=station.name,
                           codes=list(map(lambda x: QR_CODE_BASE_URL + "/waypoint/" + x, printout.codes)))

#
# Handler
#

@login_manager.user_loader
def load_user(user_id):
    if user_id is not None:
        return MasterAccount.objects.get(id=user_id)
    else:
        return None

@login_manager.unauthorized_handler
def unauthorized():
    flash(_l("Login to access that resource."))
    return redirect("/master/login")

class WaypointObject():
    def __init__(self, waypoint_id, found, track_id, station, url, code, track):
        self.waypoint_id = waypoint_id
        self.found = found
        self.track_id = track_id
        self.track = track
        self.station = station
        self.url = url
        self.code = code

def create_waypoint_object(id=None, waypoint=None, track=None, playgroup=None, station=None, stations=None, qr_code=None):
    """Create a waypoint object from the provided information and compile the necessary imformation ourselves."""
    if waypoint is None:
        if id is None:
            raise ValueError('Please provide a waypoint or its id!')
        else:
            waypoint = Waypoint.objects.with_id(id)

    if station is None:
        station = waypoint.station.fetch()

    if track is None:
        track = waypoint.track.fetch()

    # whether the waypoint was already discovered
    found = None
    if track is None:
        flash(_l("Waypoint with id {waypoint.id} does not belong to a track and is considered not yet found!").format(waypoint=waypoint))
    elif playgroup is not None:
        found = False
        if waypoint in playgroup.visited_waypoints:
            found = True

    if qr_code is None:
        qr_code = waypoint.qr_code
        if qr_code is None:
            flash(_l("QR code with id {waypoint.qr_code.id} does not exist (waypoint with id {waypoint.id} for playgroup {playgroup.name})").format(waypoint=waypoint, playgroup=playgroup))
            return WaypointObject(waypoint_id=waypoint.id,
                                  found=found,
                                  track_id=track.id,
                                  station=station,
                                  code=None,
                                  url=None,
                                  track=track)

    return WaypointObject(waypoint_id=waypoint.id,
                          found=found,
                          track_id=track.id,
                          station=station,
                          code=qr_code.code,
                          url=QR_CODE_BASE_URL + "/waypoint/" + qr_code.code,
                          track=track)

class PlaygroupObject():
    def __init__(self, playgroup_id, track_name, playgroup_name, progress, waypoint_count):
        self.playgroup_id = playgroup_id
        self.track_name = track_name
        self.playgroup_name = playgroup_name
        self.progress = progress
        self.waypoint_count = waypoint_count

    @property
    def info(self):
        if self.progress is None and self.waypoint_count is None:
            return _l("Without track")
        else:
            return _l("%(progress)s of %(waypoint_count)s waypoints", progress=self.progress+1, waypoint_count=self.waypoint_count)

def get_next_station(playgroup):
    """Get the next station for the given waypoint"""
    if playgroup is None:
        raise ValueError('Please provide a playgroup!')

    track = playgroup.track.fetch()

    visited_idxs = map(lambda x: track.stations.index(x.fetch().station), playgroup.visited_waypoints)
    progress = max(visited_idxs, default=0)

    if progress + 1 == len(track.stations):
        return None

    return track.stations[progress + 1]
