from flask import render_template, redirect, request, flash, make_response, Markup
from flask_babel import lazy_gettext as _l
from flask_login import current_user
from . import player
from .. import babel
from ..models import MasterAccount, Waypoint, QrCode, Person, Playgroup
import datetime
import uuid
from dotenv import load_dotenv
import os

load_dotenv()

QR_CODE_BASE_URL = '{}://{}{}'.format(os.getenv('PREFERRED_URL_SCHEME'), os.getenv('SERVER_NAME'), os.getenv('APP_ROOT'))


#
# Locale Initialization
#

@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(os.getenv('LANGUAGES').split(','))

#
# Routes
#


@player.route("/", methods=["GET"])
def index():
    """
    Displays a progress overview for known players
    and a generic welcome page for everyone else.
    """
    if "player_uuid" in request.cookies:
        # we should know this player! because usually we set the cookie ourselves
        player_uuid = request.cookies.get("player_uuid")
        
        try:
            # get the person from db
            person = Person.objects.get(uuid=player_uuid)
        except Person.DoesNotExist:
            flash(_l("Invalid player data. Removing your session cookie."))
            response = make_response(render_template("/player/index.html"))
            response.set_cookie("player_uuid", "", expires=0)
            return response
        
        # get the associated playgroup
        playgroup = person.playgroup.fetch()
        # get the track belonging to the playgroup
        track = playgroup.track.fetch()

        # get all waypoints of the track
        waypoints = Waypoint.objects(track=track)
        waypoint_objs = []
        # aggregate them with all important information
        waypoint_objs = list(map(lambda x: create_waypoint_object(track=track, playgroup=playgroup, waypoint=next(filter(lambda y: y.station == x, waypoints))), track.stations))

        # hint handling
        next_station_hint = ""
        next_station = get_next_station(playgroup)
        if next_station is not None:
            next_station = next_station.fetch()
            if next_station.hint is not None:
                next_station_hint = next_station.hint
        else:
            next_station_hint = _l("Congratulations! You made it to the end!")

        # show the progress!
        return render_template("/player/progress.html",
                                track_name=track.name,
                                waypoints=waypoint_objs,
                                display_hint=playgroup.solved_current_challenge or next_station is None or next_station.challenge is None,
                                next_station_hint=next_station_hint)

    # just show the index page
    return render_template("/player/index.html")


@player.route("/waypoint/<qr_code>", methods=["GET", "POST"])
def waypoint(qr_code):
    """
    Entry for all scanned QR codes. Checks whether the waypoint is already
    discovered and displays the station description or an error otherwise.
    
    Sets up unknown devices as new players in the corresponding playgroup if a
    waypoint which is first in its track is scanned.
    """
    try:
        qr_code = QrCode.objects.get(code=qr_code)
    except QrCode.DoesNotExist:
        flash(_l("Invalid waypoint."))
        return redirect("/")

    # the waypoint marked by the scanned qr code
    waypoint = Waypoint.objects.get(qr_code=qr_code)
    # the station associated with the waypoint
    station = waypoint.station.fetch()
    # the track which the waypoint belongs to
    track = waypoint.track.fetch()
    # the playgroup owning the track which the waypoint belongs to
    playgroup = Playgroup.objects.get(track=waypoint.track.fetch())
    waypoint_object = create_waypoint_object(waypoint=waypoint, track=track, playgroup=playgroup)

    # for each waypoint, find the index of the associated station in this track
    visited_idxs = map(lambda x: track.stations.index(x.fetch().station), playgroup.visited_waypoints)
    # progress is the max station index we visited
    progress = max(visited_idxs, default=0)
    # position is the index of this station
    position = track.stations.index(station)

    waypoints = []
    # all waypoints of the track
    waypoints = Waypoint.objects(track=track)
    waypoint_objs = list(map(lambda x: create_waypoint_object(track=track, playgroup=playgroup, waypoint=next(filter(lambda y: y.station == x, waypoints))), track.stations))

    if playgroup is None:
        flash(_l("An error occured. Please ask an FSR-I member to set up the corresponding playgroup."))
        return redirect("/")

    # hint handling
    next_station_hint = ""
    next_station = get_next_station(playgroup)
    if next_station is not None:
        next_station = next_station.fetch()
        if next_station.hint is not None:
            next_station_hint = next_station.hint
    else:
        next_station_hint = _l("Congratulations! You made it to the end!")

    # whether a master account is signed in
    is_master = False
    if current_user is not None:
        if current_user.get_id() is not None:
            user = MasterAccount.objects.with_id(current_user.get_id())
            if user is not None:
                is_master = user.is_authenticated
            else:
                flash(_l("Research suggests you don't exist!"))

    if is_master:
        # master wants to look at the track, we don't need to do anything else
        flash(_l("You are viewing this as master. No modifications will be made."))
        flash(Markup('{} {}'.format(_l("Next station:"), next_station_hint)))
    elif position == 0 and "player_uuid" not in request.cookies:
        # new player, who dis?
        response = make_response(render_template("/player/waypoint.html",
                                                track_name=track.name,
                                                waypoints=waypoint_objs,
                                                station_name=station.name,
                                                station_content=station.content,
                                                has_challenge=station.challenge is not None,
                                                is_master=is_master,
                                                next_station_hint=next_station_hint))

        # create a new uuid
        player_uuid = str(uuid.uuid4())

        try:
            # add the player to the db
            person = Person(uuid=player_uuid, playgroup=playgroup)
            person.save()
            if waypoint not in playgroup.visited_waypoints:
                playgroup.visited_waypoints.append(waypoint)
                playgroup.save()
            #db.session.commit()
        except:
            import traceback
            traceback.print_exc()
            #db.session.rollback()
            flash(_l("An error occured while attempting to register your device with the server."))
            return redirect("/")

        # set cookie. valid for 12 hours
        expire_date = datetime.datetime.now() + datetime.timedelta(hours=12)
        response.set_cookie("player_uuid", player_uuid, expires=expire_date)

        return response
    elif position >= 0 and "player_uuid" in request.cookies:
        player_uuid = request.cookies.get("player_uuid")
        # the player
        try:
            person = Person.objects.get(uuid=player_uuid)
        except Person.DoesNotExist:
            # known player, later waypoint
            flash(_l("Invalid player data. Removing your session cookie."))
            response = redirect("/")
            response.set_cookie("player_uuid", "", expires=0)
            return response
        
        if person.playgroup.fetch() != playgroup:
            flash(_l("You're in a different track/playgroup."))
            return redirect("/")

        if position > progress + 1:
            # waypoint is not yet reachable
            flash(_l("You're not allowed to visit this waypoint."))
            return redirect("/")

        if waypoint not in playgroup.visited_waypoints:
            try:
                playgroup.visited_waypoints.append(waypoint)
                playgroup.solved_current_challenge = False
                playgroup.save()
                #db.session.commit()
            except:
                #db.session.rollback()
                flash(_l("An error occured while attempting to save your game progress."))
                return redirect("/")
    else:
        if position > progress:
            flash(_l("You're not allowed to visit this waypoint."))
            return redirect("/")

    return render_template("/player/waypoint.html",
                           track_name=track.name,
                           waypoints=waypoint_objs,
                           station_name=station.name,
                           station_content=station.content,
                           has_challenge=station.challenge is not None,
                           is_master=is_master,
                           next_station_hint=next_station_hint)


@player.route("/waypoint/<qr_code>/challenge", methods=["GET", "POST"])
def challenge(qr_code):
    # whether a master account is signed in
    is_master = False
    if current_user.get_id() is not None:
        user = MasterAccount.objects.with_id(current_user.get_id())
        if user is not None:
            is_master = user.is_authenticated

    if "player_uuid" not in request.cookies and not is_master:
        flash(_l("You're not allowed to try this challenge."))
        return redirect("/waypoint/{}".format(qr_code))

    try:
        qr_code = QrCode.objects.get(code=qr_code)
    except QrCode.DoesNotExist:
        flash(_l("Invalid waypoint."))
        return redirect("/")

    # the waypoint marked by the scanned qr code
    waypoint = Waypoint.objects.get(qr_code=qr_code)
    # the station associated with the waypoint
    station = waypoint.station.fetch()
    # the playgroup owning the track which the waypoint belongs to
    playgroup = Playgroup.objects.get(track=waypoint.track.fetch())
    # the track which the waypoint belongs to
    track = playgroup.track.fetch()
    waypoint_object = create_waypoint_object(waypoint=waypoint, track=track, playgroup=playgroup)

    # hint handling
    next_station_hint = ""
    next_station = get_next_station(playgroup)

    if next_station is not None:
        next_station = next_station.fetch()
        if next_station.hint is not None:
            next_station_hint = next_station.hint
    else:
        next_station_hint = _l("Congratulations! You made it to the end!")

    if playgroup is None:
        flash(_l("An error occured. Please ask an FSR-I member to set up the corresponding playgroup."))
        return redirect("/")

    if station.challenge is None:
        flash(_l("There is no challenge for this station. Onward!"))
        # TODO: show hint
        return redirect("/")
    else:
        # the challenge for the station
        challenge = station.challenge.fetch()
        if challenge is None:
            flash(_l("An error occured. Please ask an FSR-I member to check the challenge for this station."))
            return redirect("/")

    # for each waypoint, find the index of the associated station in this track
    visited_idxs = map(lambda x: track.stations.index(x.fetch().station), playgroup.visited_waypoints)
    # progress is the max station index we visited
    progress = max(visited_idxs, default=0)
    # position is the index of this station
    position = track.stations.index(station)

    if progress > position:
        # this is an old waypoint, consider the challenge done
        solved = True
    elif progress == position:
        solved = playgroup.solved_current_challenge
    else:
        # this should never be reachable. anyways, the future challenge would not be solved already
        solved = False

    if is_master:
        # master wants to look at the challenge, we don't need to do anything else
        flash(_l("You are viewing this as master. No modifications will be made."))

        if "S0LVED" in request.form and request.method == "POST":
            flash(_l("Challege secret was correct."))
    else:
        player_uuid = request.cookies.get("player_uuid")
        try:
            # the player
            person = Person.objects.get(uuid=player_uuid)
        except Person.DoesNotExist:
            flash(_l("Invalid player data. Removing your session cookie."))
            response = redirect("/")
            response.set_cookie("player_uuid", "", expires=0)
            return response

        if person.playgroup.fetch() != playgroup:
            flash(_l("You're in a different track/playgroup."))
            return redirect("/")

        if progress < position - 1:
            # waypoint is not yet reachable
            flash(_l("You're not allowed to visit this waypoint."))
            return redirect("/")

        if "S0LVED" in request.form and request.method == "POST":
            # they beat the challenge! yaaay!
            if progress == position:
                # oh and it was also the current challenge! wow!
                # we check this so there is no cheating by solving old challenges to set this flag to true
                flash(_l("Correct! Now go for the next one!"))
                try:
                    playgroup.solved_current_challenge = True
                    playgroup.save()
                    #db.session.commit()
                except:
                    #db.session.rollback()
                    flash(_l("An error occured while saving your challenge progress."))
                    return redirect("/waypoint/{}".format(qr_code))
            else:
                flash(_l("Correct! (Challege was already solved)"))

            flash(Markup('{} {}'.format(_l("Next station:"), next_station_hint)))
            return redirect("/")

        if "skip_btn" in request.form and request.method == "POST":
            if not solved:
                # LIARS!!!
                return redirect("/waypoint/{}".format(qr_code))

            # they beat the challenge before.
            flash(Markup('{} {}'.format(_l("Next station:"), next_station_hint)))
            return redirect("/")

    return render_template("/player/challenge.html",
                           track_name=track.name,
                           challenge=challenge,
                           solved=solved,
                           next_station_hint=next_station_hint,
                           is_master=is_master)


class WaypointObject():
    def __init__(self, waypoint_id, found, track_id, station, url, code, position):
        self.waypoint_id = waypoint_id
        self.found = found
        self.track_id = track_id
        self.station = station
        self.url = url
        self.code = code
        self.position = position + 1

def create_waypoint_object(id=None, waypoint=None, track=None, playgroup=None, station=None, stations=None, qr_code=None):
    """Create a waypoint object from the provided information and compile the necessary imformation ourselves."""
    if waypoint is None:
        if id is None:
            raise ValueError('Please provide a waypoint or its id!')
        else:
            waypoint = Waypoint.objects.with_id(id)

    if station is None:
        station = waypoint.station.fetch()

    if track is None:
        track = waypoint.track.fetch()

    # whether the waypoint was already discovered
    found = None
    if track is None:
        flash(_l("Waypoint with id {waypoint.id} does not belong to a track and is considered not yet found!").format(waypoint=waypoint))
    elif playgroup is not None:
        found = False
        if waypoint in playgroup.visited_waypoints:
            found = True

    if qr_code is None:
        qr_code = waypoint.qr_code
        if qr_code is None:
            return WaypointObject(waypoint_id=waypoint.id,
                                  found=found,
                                  track_id=track.id,
                                  station=station,
                                  code=None,
                                  url=None,
                                  position=track.stations.index(station))

    return WaypointObject(waypoint_id=waypoint.id,
                          found=found,
                          track_id=track.id,
                          station=station,
                          code=qr_code.code,
                          url=QR_CODE_BASE_URL + "/waypoint/" + qr_code.code,
                          position=track.stations.index(station))


def get_next_station(playgroup):
    """Get the next station for the given waypoint"""
    if playgroup is None:
        raise ValueError('Please provide a playgroup!')

    track = playgroup.track.fetch()

    visited_idxs = map(lambda x: track.stations.index(x.fetch().station), playgroup.visited_waypoints)
    progress = max(visited_idxs, default=0)

    if progress + 1 == len(track.stations):
        return None

    return track.stations[progress + 1]
