from flask import Flask
from flask_babel import Babel
from flask_login import LoginManager
from flask_qrcode import QRcode
from flask_mongoengine import MongoEngine
from dotenv import load_dotenv
import os

load_dotenv()

mongo = MongoEngine()
babel = Babel()
login_manager = LoginManager()
qr_code = QRcode()

import app.models as models

def create_app():
    app = Flask(__name__)
    app.secret_key = os.getenv('SECRET_KEY')
    app.url_map.strict_slashes = False

    app.config['PREFERRED_URL_SCHEME'] = os.getenv('PREFERRED_URL_SCHEME')
    app.config['SERVER_NAME'] = os.getenv('SERVER_NAME')
    app.config['APPLICATION_ROOT'] = os.getenv('APP_ROOT')
    app.config['BABEL_DEFAULT_LOCALE'] = os.getenv('DEFAULT_LOCALE')
    app.config['BABEL_TRANSLATION_DIRECTORIES'] = '../translations'

    app.config['MONGODB_SETTINGS'] = {
        'db': os.getenv('DATABASE_NAME'),
        'host': os.getenv('DATABASE_HOST'),
        'username': os.getenv('DATABASE_USERNAME'),
        'password': os.getenv('DATABASE_PASSWORD'),
        'port': int(os.getenv('DATABASE_PORT'))
    }

    mongo.init_app(app)

    with app.app_context():
        __create_initial_db_entries()
    
    babel.init_app(app)
    login_manager.init_app(app)
    qr_code.init_app(app)

    from .player import player as player_blueprint
    app.register_blueprint(player_blueprint)

    from .master import master as master_blueprint
    app.register_blueprint(master_blueprint)

    return app

def __create_initial_db_entries():
    from app.models import MasterAccount

    if MasterAccount.objects.count() != 0:
        return

    account = MasterAccount(username="admin")
    account.set_password("admin")
    account.save()
